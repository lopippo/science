Task: Physics
Install: false
Description: Debian Science Physics packages
 This metapackage will install Debian Science packages related to Physics.
 You might also be interested in the debtag field::physics and, depending on
 your focus, in education-physics metapackage.

Recommends: cernlib, geant321, paw++, paw
Suggests: paw-demos

Recommends: gpaw

Recommends: clhep
WNPP: 636972
Responsible: Lifeng Sun <lifongsun@gmail.com>
Homepage: http://proj-clhep.web.cern.ch/proj-clhep/
License: GPL-3, LGPL-3
Language: C++
Pkg-Description: Class Library for High Energy Physics
 The CLHEP package is a set of High Energy Physics specific foundation
 and utility classes such as random generators, physics vectors,
 geometry and linear algebra.

Recommends: geant4

Recommends: abinit | mpqc | openmx | psi3
Why: Abinitio Quantum mechanics (As psi3 doesn't do DFT, maybe it should be excluded)

Recommends: gerris

Recommends: feynmf

Recommends: openfoam

Recommends: fityk

Recommends: tessa | tessa-mpi

Recommends: science-numericalcomputation
Why: numerical programming environments similar to Matlab/IDL


Recommends: oce-draw

Recommends: science-electronics

Recommends: v-sim

Recommends: gpiv, gpivtools

Recommends: maxima
Why: Symbolic maths

Recommends: python3-sympy
Why: Symbolic maths

Recommends: axiom
Why: Symbolic maths

Suggests: science-statistics, science-mathematics

Recommends: meshlab

Suggests: openctm-tools

Recommends: cadabra

Recommends: drawxtl

Recommends: opticalraytracer

Recommends: ghkl

Recommends: python3-deap

Suggests: feel++-apps
Why: Partial differential equation library, FEA, CFD

Recommends: etsf-io

Recommends: pymca

Recommends: quantum-espresso

Recommends: gwyddion

Recommends: blzpack
Homepage: http://crd.lbl.gov/~osni/#Software
WNPP: 435394
License: BSD
Responsible: Ondrej Certik <ondrej@certik.cz>
Pkg-Description: library for solving large sparse eigenproblems
 BLZPACK (for Block LancZos PACKage, release 04/00) is a standard Fortran 77
 implementation of the block Lanczos algorithm intended for the solution of the
 standard eigenvalue problem Ax=µx or the generalized eigenvalue problem
 Ax=µBx, where A and B are real, sparse symmetric matrices, µ an eigenvalue and
 x an eigenvector.
 .
 The development of this eigensolver was motivated by the need to solve large,
 sparse, generalized problems from free vibration analyses in structural
 engineering. Several upgrades were performed afterwards aiming at the solution
 of eigenvalues problems from a wider range of applications.
 .
 Documentation: user's guide, technical report and comprehensive bibliography.
 .
 Install this package if you need to compile or link against BLZPACK.

Recommends: gate
Homepage: http://opengatecollaboration.healthgrid.org/
WNPP: 431425
License: LGPL
Responsible: Nicolas Spalinger <nicolas.spalinger@healthgrid.org>
Pkg-Description: Geant4 Application for Emission Tomography
 GATE incorporates the Geant4 libraries in a modular, versatile, and
 scripted simulation toolkit which is adapted to the field of nuclear
 medicine both in PET (Positron Emission Tomography) and SPECT (Single
 Photon Emission Computer Tomography). It allows the accurate description
 of time-dependent phenomena such as source or detector movement and
 source decay kinetics. The ability to synchronize all time-dependent
 components allows a coherent description of the acquisition process. It
 makes it possible to perform realistic simulations of data acquisitions
 in time.

Recommends: jfreemesh
Homepage: http://dev.artenum.com/projects/JFreeMesh
License: QPL
Pkg-Description: 3D mesh library in Java
 JFreeMesh is a 3D mesh library written in Java and providing a high level API
 for mesh manipulation. JFreeMesh is based on a descending mesh data structure
 but simulate a full connectivity mesh by providing optimized method to access
 to any upper mesh element by using the global mesh methods. Therefore,
  JFreeMesh allows to load a large amount of mesh elements for a small memory
 foot print. JFreeMesh comes with a default mesh loader based on the GMSH file
 format and provides, through the JFreeMesh-VTK package, an exporter to VTK.
 .

Recommends: spis
Homepage: http://dev.spis.org/projects/spine/home/spis
License: GPL
Pkg-Description: Software toolkit for spacecraft-plasma interactions modelling
 JSPIS stands for Spacecraft Plasma Interaction System. SPIS project aims at
 developing a software toolkit for spacecraft-plasma interactions modelling.


Recommends: espresso++
Homepage: http://espresso.scai.fraunhofer.de/
License: Not yet known (hopefully free)
Pkg-Description: Extensible Simulation Package for Research on Soft matter
 ESPResSo is a highly versatile software package for the scientific
 simulation and analysis of coarse-grained atomistic or bead-spring
 models as they are used in soft matter research, with emphasis on
 charged systems.

Recommends: cp2k


X-Removed: Packages removed from Debian

Recommends: octaviz
Pkg-URL: http://snapshot.debian.org/package/octaviz/
Homepage: http://octaviz.sourceforge.net/
Pkg-Description: 3D visualization system for Octave
 Octaviz is a visualization system for Octave. It is a wrapper that
 makes all VTK classes accessible from within Octave using the same
 object-oriented syntax as in C++ or Python. Octaviz also provides
 high-level functions for 2D and 3D visualization. Using those
 functions, most common visualization tasks (3D surface plots, contour
 plots etc) can be accomplished without any knowledge about VTK.
Remark: Removed from Debian
 This package was removed from Debian but some versions are available
 from http://snapshot.debian.org/
 .
 Reasons are given here: http://bugs.debian.org/535537

Recommends: root-system

Recommends: libroot-tmva-dev, libroot-montecarlo-vmc-dev, libroot-math-mlp-dev
X-Comment: Somebody injected the libraries from the ROOT system explicitely
 here for physics - so these are included since root-system is packaged again
X-Suggests: root-plugin-fftw3, libroot-mathmore5.18
 In case these packages will be created in root-system these need to be
 added here as well.

Recommends: fdmnes
Pkg-URL: http://people.debian.org/~tille/packages/fdmnes/

Recommends: pyfr

Suggests: evolver-ogl | evolver-nox

Recommends: toulbar2
