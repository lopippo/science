Task: Statistics
Install: true
Description: Debian Science Statistics packages
 This metapackage is part of the Debian Pure Blend "Debian Science"
 and installs packages related to statistics.  This task is a general
 task which might be useful for any scientific work.  It depends from
 a lot of R packages as well as from other tools which are useful to
 do statistics.  Moreover the Science Mathematics task is suggested
 to optionally install all mathematics related software.

Recommends: elpa-ess, littler, r-recommended, rkward
Why: R utilities or frontends

Recommends: r-cran-mapproj, r-cran-abind, r-cran-arm, r-cran-bayesm,
            r-cran-boot, r-cran-cairodevice, r-cran-ca, r-cran-car, r-cran-caret, r-cran-chron,
            r-cran-cluster, r-cran-coda, r-cran-codetools,
            r-cran-date, r-cran-dbi,
            r-cran-eco, r-cran-effects, r-cran-erm,
            r-cran-fbasics, r-cran-fcopulae, r-cran-fecofin, r-cran-fextremes,
            r-cran-fmultivar, r-cran-foptions, r-cran-foreign, r-cran-fportfolio,
            r-cran-gam, r-cran-gdata, r-cran-geepack, r-cran-glmnet, r-cran-gmodels,
            r-cran-goftest, r-cran-gplots, r-cran-gregmisc, r-cran-gtools,
            r-cran-hdf5, r-cran-hmisc, r-cran-its, r-cran-kernsmooth, r-cran-lattice,
            r-cran-latticeextra, r-cran-lavaan, r-cran-lhs, r-cran-lme4, r-cran-lmtest,
            r-cran-mapdata, r-cran-maps, r-cran-maptree, r-cran-matchit, r-cran-matrix,
            r-cran-mcmcpack, r-cran-mgcv, r-cran-misc3d, r-cran-modelmetrics, r-cran-mnp,
            r-cran-multcomp, r-cran-mvtnorm, r-cran-nlme, r-cran-pscl,
            r-cran-psy, r-cran-qtl, r-cran-quadprog, r-cran-qvcalc,
            r-cran-randomfieldsutils, r-cran-raschsampler, r-cran-rcmdr,
            r-cran-relimp, r-cran-rggobi, r-cran-rgl, r-cran-rgtk2,
            r-cran-rmpi, r-cran-rms, r-cran-rmysql, r-cran-rodbc,
            r-cran-rpart, r-cran-rpvm, r-cran-rquantlib, r-cran-rserve,
            r-cran-rsprng,
            r-cran-sandwich, r-cran-sem, r-cran-sm, r-cran-snow,
            r-cran-strucchange, r-cran-survival, r-cran-timedate, r-cran-tkrplot,
            r-cran-tseries, r-cran-vgam, r-cran-mass, r-cran-xml,
            r-cran-zelig, r-cran-zoo
Why: R CRAN

Comment: r-cran-rcompgen, r-cran-e1071
         These packages formerly mentioned above do not seem to exist.

Recommends: r-cran-msm, r-cran-amore, r-cran-teachingdemos

Recommends: r-cran-expm

Recommends: r-cran-ggplot2, r-cran-reshape2

Recommends: psignifit

Recommends: python3-scipy
Why: Provides scipy.stats module

Suggests: r-cran-spc

Suggests: r-cran-mlmrev

Recommends: r-cran-vcd, r-cran-vcdextra

Suggests: r-cran-gnm

Recommends: r-cran-corpcor

Recommends: r-cran-bradleyterry2

Suggests: r-cran-brglm

Recommends: r-cran-estimability, r-cran-elliptic, r-cran-logspline, r-cran-lsmeans, r-cran-modeltools

Recommends: r-cran-rinside, r-cran-pbapply, r-cran-pbivnorm, r-cran-contfrac, r-cran-desolve, r-cran-hypergeo, r-cran-coin, r-cran-bayesfactor, r-cran-afex, r-cran-bms, r-cran-conting

Recommends: r-cran-permute

Recommends: python3-statsmodels

Suggests: science-mathematics

Recommends: r-cran-spam

Recommends: r-cran-spatstat, r-cran-deldir

Recommends: r-cran-fastcluster

Recommends: r-cran-statmod

Recommends: python3-openturns, python3-persalys, persalys
Why: Uncertainty quantification in numerical simulation

Recommends: python3-pandas

Recommends: r-cran-ecodist, r-cran-energy, r-cran-mvnormtest

Recommends: r-cran-rsdmx

Suggests: libtamuanova-dev

X-Removed: Packages removed from Debian

Recommends: r-cran-acepack

Recommends: r-cran-fcalendar
Pkg-URL: http://snapshot.debian.org/binary/r-cran-fcalendar/
Homepage: http://www.Rmetrics.org
Pkg-Description: GNU R package for financial engineering -- fCalendar
 This package of functions for financial engineering and computational
 finance is part of Rmetrics, a collection of packages written and
 compiled by Diethelm Wuertz.
 .
 fCalendar provides basic time and date manipulations, holiday
 calculators and other tools. Its code was previously provided within
 the fBasics package from which it has been split off.
Remark: Removed from Debian
 This package was removed from Debian but some versions are available
 from http://snapshot.debian.org/
 .
 Reasons are given here: http://bugs.debian.org/554513

Recommends: rstudio

Suggests: r-cran-blockmodeling

Suggests: r-cran-catools

Suggests: r-cran-fastmatch

Suggests: r-cran-futile.logger, r-cran-futile.options

Suggests: r-cran-igraph

Suggests: r-cran-lambda.r

Suggests: r-cran-magrittr

Suggests: r-cran-nnls

Suggests: r-cran-r.oo, r-cran-r.utils

Recommends: apophenia-bin

Suggests: libapophenia2-dev

Suggests: r-cran-bbmle

Suggests: r-cran-ellipse

Suggests: r-cran-irlba

Suggests: r-cran-luminescence

Suggests: r-cran-minpack.lm

Recommends: r-cran-nmf

Suggests: r-cran-princurve

Suggests: r-cran-rngtools

Recommends: pspp

Recommends: datamash

Recommends: python3-bayespy

Suggests: node-shiny-server-client

Recommends: libdistlib-java
